#include <iostream>
using namespace std;

void printArray(int size, int arr[]) {
    cout << "size of arr: " << sizeof(arr) << endl;

    for (int i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
}


int main() {

    const int size = 5;
    int arr[size] = {3, 5, 7, 9, 11};
    int* p = arr;

    cout << "size of arr: " << sizeof(arr) << endl;
    printArray(size, arr);


	return 0;
}

#include <iostream>
using namespace std;

void printString(const char* c) {
    while (*c != '\0') {
        cout << *c;
        c++;
    }
    cout << endl;
}

int main() {

    // char* course = "CSE24";
    char course[] = {'C', 'S', 'E', '2', '4', '\0'};

    string semester = "Spring 2023";

    printString(course);
    printString(semester.c_str());


	return 0;
}

#include <iostream>
using namespace std;

int main() {

    const int size = 5;
    int **p = new int*[size];

    for (int i = 0; i < size; i++) {
        p[i] = new int[size];
        // *(p + i) = new int[size];
    }

    p[0][0] = 1;
    p[1][1] = 3;
    // *(*(p + 1) + 3) = 3;
    p[2][2] = 5;
    p[3][3] = 7;
    p[4][4] = 9;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout << p[i][j] << " ";
        }
        cout << endl;
    }

    for (int i = 0; i < size; i++) {
        delete[] p[i];
        // delete[] *(p + i);
    }

    delete[] p;

	return 0;
}

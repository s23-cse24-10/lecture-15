#include <iostream>
using namespace std;

std::string decode(long s){
    std::string result = "";

    void* p = &s;
    
    for (int i = 0; i < 8; i++) {
        result += *(char*)p;
        p = (char*)p + 1;
    }

    return result;
}

int main() {
    long x = 5928237209899323203;
    std::string s = decode(x);
    
    cout << s << endl;
	return 0;
}
